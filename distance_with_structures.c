//WAP to find the distance between two points using structures and 4 functions.

#include <stdio.h>
#include <math.h>
#include <string.h>
 
struct Point {
	int x;
	int y;
};
int main( ) {

   struct Point p1;     
   struct Point p2;
   
   int x1=p1.x;
   int y1=p1.y;
   
   int x2=p2.x;
   int y2=p2.y;
   
   printf("Enter point1 x axis value: ");
   scanf("%d",&x1);
   
   printf("Enter point1 y axis value: ");
   scanf("%d",&y1);
   
   printf("Enter point2 x axis value: ");
   scanf("%d",&x2);
   
   printf("Enter point2 y axis value: ");
   scanf("%d",&y2);
   
   int X = (x2-x1);
   int Y = (y2-y1);
   
   float distance = sqrt(X*X + Y*Y);
   
   printf("Distance between two coordinate is : %f", distance);
   
   }
