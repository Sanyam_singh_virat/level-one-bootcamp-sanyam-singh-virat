//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
#include<math.h>

double volume(double h, double d, double b)
{
   double multi= h*d*b;
   float divide = d/b;
   float result = (multi+divide)/3;
   
   return result;
   
}
int main ()
{

  double h, d, b;
  float vol;
  printf ("enter the height:-");
  scanf ("%lf", &h);

  printf ("enter the depth:-");
  scanf ("%lf", &d);

  printf ("enter the base:- ");
  scanf ("%lf", &b);

  vol = volume(h,d,b);
  printf ("volume of tromboloid is:- %lf", vol);
  return 0;
}