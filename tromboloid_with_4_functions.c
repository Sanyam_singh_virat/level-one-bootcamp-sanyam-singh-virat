//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
#include <math.h>

double input()
{
   double num;
   scanf("%lf",&num);
   return num;
}

double volume(double h, double d, double b)
{
   double multi= h*d*b;
   float divide = d/b;
   float result = (multi+divide)/3;
   return result;
}

void result(double res){
	printf("Volume of tromboloid is:%lf", res);
}

int main()
{
 double h,d,b;
 float vol;
 printf("Enter height:");
 h=input();
 
 printf("Enter depth:");
 d=input();
 
 printf("Enter base:");
 b=input();
 
 double res=volume(h,d,b);
 result(res);
 return 0;
    
}
