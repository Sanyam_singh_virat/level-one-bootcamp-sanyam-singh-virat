//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>

double input()
{
   double num;
   scanf("%lf",&num);
   return num;
}

double volume(double x1, double x2, double y1, double y2)
{
   double x= (x2-x1)*(x2-x1);
   double y = (y2-y1)*(y2-y1);
   double xy = x+y;
   float result = sqrt(xy);
   return result;
}

void result(double res)
{
	printf("Distance between two coordinate is :%lf", res);
}

int main()
{
 double x1,x2,y1,y2;
 float dist;
 printf("Enter x1:");
 x1=input();
 
 printf("Enter x2:");
 x2=input();

 printf("Enter y1:");
 y1=input();
 
 printf("Enter y2:");
 y2=input();
 
 double res=volume(x1,x2,y1,y2);
 result(res);
 return 0;
    
}
